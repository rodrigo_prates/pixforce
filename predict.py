from ClassImage import ClassImage
import os


classifier = ClassImage()
dataset_folder = os.path.sep.join([os.getcwd(), 'dataset'])
test_folder = os.path.sep.join([dataset_folder, 'test'])
batchsize = 8
num_test_files = 1001
modelname = 'VGG16'
checkpoint = os.path.sep.join([os.getcwd(), 'models', 'checkpoints', modelname])
classifier.predict(modelname, checkpoint, test_folder, batchsize, num_test_files, 2)

#        precision    recall  f1-score   support

#           0       0.66      0.87      0.75       660
#           1       0.32      0.12      0.18       341

#    accuracy                           0.61      1001
#   macro avg       0.49      0.49      0.46      1001
#weighted avg       0.54      0.61      0.55      1001

#[[574  86]
# [300  41]]