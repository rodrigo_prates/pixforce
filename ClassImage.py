#Class with enough methods to train a binary classifier.
#For use you can check the script examples:
#gen_data.py: generate images
#split_data.py: split images in train/validation/test sets
#train.py: train a model with transfer learning and fine-tuning 

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import MobileNet, MobileNetV2, VGG16, VGG19, Xception, imagenet_utils, ResNet50, ResNet50V2, InceptionV3
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import SGD, Adam, Adadelta
from tensorflow.keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, CSVLogger, LearningRateScheduler, ReduceLROnPlateau
from sklearn.metrics import classification_report
#from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import os
from os import listdir, makedirs
from os.path import isfile, join, isdir, basename
import shutil
import cv2
from sklearn.model_selection import train_test_split, KFold
from sklearn.metrics import confusion_matrix
from imgaug import augmenters as iaa
import imageio

#from imgaug import augmenters as iaa
#import imageio

#https://www.pyimagesearch.com/2019/06/03/fine-tuning-with-keras-and-deep-learning/
#https://www.pyimagesearch.com/2020/04/27/fine-tuning-resnet-with-keras-tensorflow-and-deep-learning/
class ClassImage:

    MODELS = {
      "VGG16": VGG16,
      "MobileNet": MobileNet,
      "MobileNetV2": MobileNetV2,
      "ResNet50": ResNet50,
      "ResNet50V2": ResNet50V2,
      "InceptionV3": InceptionV3
    }
    
    def __init__(self):
        pass

    #Generate images with target class in their names
    def gen_data(self, data_folder, image_folder):

        if os.path.isdir(image_folder) and os.listdir(image_folder):
            shutil.rmtree(image_folder)

        if not isdir(image_folder):
            makedirs(image_folder)

        num_true_class = 0
        num_false_class = 0

        X_files = sorted([join(data_folder, f) for f in listdir(data_folder) if isfile(join(data_folder, f)) and 'trainX' in f])
        Y_files = sorted([join(data_folder, f) for f in listdir(data_folder) if isfile(join(data_folder, f)) and 'trainY' in f])

        for fx,fy in zip(X_files,Y_files):
            print(fx)
            samples_x = np.load(fx)
            samples_y = np.load(fy)
            for i in range(0, len(samples_x)):
                true_class = True if samples_y[i][0] == 0 else False
                dim = (224, 224)
                img = cv2.resize(samples_x[i], dim)
                base_file_name = basename(fx).split('.')[0] + '_' + str(i)
                if true_class:
                    num_true_class += 1
                    cv2.imwrite(join(image_folder, base_file_name + '_1.png'), cv2.cvtColor(img.astype('uint8'), cv2.COLOR_RGB2BGR))
                else:
                    num_false_class += 1
                    cv2.imwrite(join(image_folder, base_file_name + '_0.png'), cv2.cvtColor(img.astype('uint8'), cv2.COLOR_RGB2BGR))
        
        return  num_true_class, num_false_class

    #Split data in train/validation/test sets
    def split_data_from_folder(self, image_folder, dataset_folder, trainsize=0.75, do_augmentation=False, 
                                cls_to_aug='1', aug_times=2):
        if not isdir(dataset_folder):
            makedirs(dataset_folder)

        image_list = []
        class_list = []
        for filename in sorted(os.listdir(image_folder)):
            image_list.append(filename)
            class_list.append( filename.split('.')[0].split('_')[-1] )

        test_size = 1-trainsize
        input_train, input_test, output_train, output_test = train_test_split(image_list, class_list, test_size=test_size)
        input_test, input_val, output_test, output_val = train_test_split(input_test, output_test, test_size=0.5)
        self.organize_dataset(dataset_folder, 'train', image_folder, input_train, output_train, do_augmentation)
        self.organize_dataset(dataset_folder, 'validation', image_folder, input_val, output_val)
        self.organize_dataset(dataset_folder, 'test', image_folder, input_test, output_test)

    #Copy file to specific folders for data spliting
    def organize_dataset(self, dataset_folder, dataset_type, image_folder, image_list, class_list, 
                        do_augmentation=False, cls_to_aug='1', aug_times=2):
        classes = np.unique( np.array(class_list) )        
        for cls in classes:
            dataset_type_folder = os.path.sep.join([dataset_folder, dataset_type, cls])

            if isdir(dataset_type_folder) and listdir(dataset_type_folder):
                shutil.rmtree(dataset_type_folder)

            if not os.path.isdir(dataset_type_folder):
                os.makedirs(dataset_type_folder)

        for i in range(0, len(image_list)):
            origfilepath = os.path.sep.join([image_folder, image_list[i]])
            newfilepath = os.path.sep.join([ os.path.sep.join([dataset_folder, dataset_type, class_list[i]]), image_list[i]])
            shutil.copy2(origfilepath, newfilepath)

        if do_augmentation:
            imagespath = os.path.sep.join([dataset_folder, dataset_type, cls_to_aug])
            self.geometric_augment_images(imagespath, aug_times)
            

    def geometric_augment_images(self, imagespath, num_times=2):
        for filename in os.listdir(imagespath):
            images = []
            im = cv2.imread(os.path.sep.join([imagespath, filename]))
            images.append(im)
            images = np.array(images)
            for i in range(1, num_times):
                seq = iaa.OneOf([
                    iaa.Flipud(0.5),
                    iaa.Affine(rotate=(-45, 45),
                        order=[0, 1],
                        scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}
                    )
                ])
                aug_images = seq(images=images)
                output_name = os.path.splitext(filename)[0] + '_g' + str(i) + '.png'
                cv2.imwrite( os.path.sep.join([imagespath, output_name]), aug_images[0] )

    #training model with transfer learning and fine_tuning
    def training(self, dataset_folder, modelname, batchsize, numepochs_head, numepochs_body, num_classes, num_train_files, num_val_files, num_test_files):

        netmodel = self.MODELS[modelname]
        
        trainAug = ImageDataGenerator(
	        rotation_range=30,
	        zoom_range=0.15,
	        width_shift_range=0.2,
	        height_shift_range=0.2,
	        shear_range=0.15,
	        horizontal_flip=True,
	        fill_mode="nearest")

        valAug = ImageDataGenerator()
        mean = np.array([123.68, 116.779, 103.939], dtype="float32")
        trainAug.mean = mean
        valAug.mean = mean

        trainPath = os.path.sep.join([dataset_folder, 'train'])
        trainGen = trainAug.flow_from_directory(
            trainPath,
	        class_mode="binary",
	        target_size=(224, 224),
	        color_mode="rgb",
	        shuffle=True,
	        batch_size=batchsize)

        valPath = os.path.sep.join([dataset_folder, 'validation'])
        valGen = valAug.flow_from_directory(
            valPath,
	        class_mode="binary",
	        target_size=(224, 224),
	        color_mode="rgb",
	        shuffle=False,
	        batch_size=batchsize)

        testPath = os.path.sep.join([dataset_folder, 'test'])
        testGen = valAug.flow_from_directory(
            testPath,
	        class_mode="binary",
	        target_size=(224, 224),
	        color_mode="rgb",
	        shuffle=False,
	        batch_size=batchsize)

        baseModel = netmodel(weights="imagenet", include_top=False, input_tensor=Input(shape=(224, 224, 3)))
        
        headModel = baseModel.output
        headModel = Flatten(name="flatten")(headModel)
        headModel = Dense(512, activation="relu")(headModel)
        headModel = Dropout(0.5)(headModel)
        headModel = Dense(num_classes, activation="softmax")(headModel)

        model = Model(inputs=baseModel.input, outputs=headModel)

        for layer in baseModel.layers:
            layer.trainable = False

        print("[INFO] compiling model...")
        #opt = SGD(lr=1e-4, momentum=0.9)
        #opt = SGD(lr=1e-4, momentum=0.9, decay=1e-4 / numepochs)
        model.compile(loss="binary_crossentropy", optimizer='adadelta', metrics=["accuracy"]) #optimizer='adam', 'adadelta'

        earlystopping_callback = EarlyStopping(monitor='val_loss', patience=10, mode='min')

        checkpointpath = os.path.sep.join([os.getcwd(), 'models', 'checkpoints'])
        filepath= os.path.sep.join([os.getcwd(), 'models', 'checkpoints', modelname])
        if not isdir(checkpointpath):
            makedirs(checkpointpath)
        checkpoint_callback = ModelCheckpoint(filepath = filepath, save_best_only=True, monitor='val_loss', save_weights_only=True)

        callbacks = [
            earlystopping_callback, checkpoint_callback
        ]

        print("[INFO] training head...")
        H = model.fit(x=trainGen, steps_per_epoch=num_train_files // batchsize, validation_data=valGen, 
            validation_steps=num_val_files // batchsize, epochs=numepochs_head, callbacks=callbacks)

        print("[INFO] evaluating after fine-tuning network head...")
        testGen.reset()
        predIdxs = model.predict(x=testGen, steps=(num_test_files // batchsize) + 1)
        predIdxs = np.argmax(predIdxs, axis=1)
        print(classification_report(testGen.classes, predIdxs, target_names=testGen.class_indices.keys()))
        print(confusion_matrix(testGen.classes, predIdxs))

        trainGen.reset()
        valGen.reset()

        for layer in baseModel.layers[15:]:
	        layer.trainable = True

        for layer in baseModel.layers:
	        print("{}: {}".format(layer, layer.trainable))

        print("[INFO] re-compiling model...")
        #opt = SGD(lr=1e-4, momentum=0.9)
        model.compile(loss="binary_crossentropy", optimizer='adadelta', metrics=["accuracy"])

        H = model.fit(x=trainGen, steps_per_epoch=num_train_files // batchsize, 
            validation_data=valGen, validation_steps=num_val_files // batchsize, epochs=numepochs_body, callbacks=callbacks)

        print("[INFO] evaluating after fine-tuning network...")
        testGen.reset()
        predIdxs = model.predict(x=testGen, steps=(num_test_files // batchsize) + 1)
        predIdxs = np.argmax(predIdxs, axis=1)
        print(classification_report(testGen.classes, predIdxs, target_names=testGen.class_indices.keys()))
        print(confusion_matrix(testGen.classes, predIdxs))

    def predict(self, modelname, checkpoint, inputfolder, batchsize, num_test_files, num_classes):
        
        netmodel = self.MODELS[modelname]
        baseModel = netmodel(weights="imagenet", include_top=False, input_tensor=Input(shape=(224, 224, 3)))
        headModel = baseModel.output
        headModel = Flatten(name="flatten")(headModel)
        headModel = Dense(512, activation="relu")(headModel)
        headModel = Dropout(0.5)(headModel)
        headModel = Dense(num_classes, activation="softmax")(headModel)
        model = Model(inputs=baseModel.input, outputs=headModel)

        model.compile(loss="binary_crossentropy", optimizer='adadelta', metrics=["accuracy"])
        print('chaeckpints_path: ', checkpoint)
        model.load_weights(checkpoint)

        testAug = ImageDataGenerator()
        mean = np.array([123.68, 116.779, 103.939], dtype="float32")
        testAug.mean = mean

        testGen = testAug.flow_from_directory(
            inputfolder,
	        class_mode="binary",
	        target_size=(224, 224),
	        color_mode="rgb",
	        shuffle=False,
	        batch_size=batchsize)

        predIdxs = model.predict(x=testGen, steps=(num_test_files // batchsize) + 1)
        predIdxs = np.argmax(predIdxs, axis=1)
        print('classification report: (precision vs recall)')
        print(classification_report(testGen.classes, predIdxs, target_names=testGen.class_indices.keys()))
        print('confusion matrix: ')
        print(confusion_matrix(testGen.classes, predIdxs))
