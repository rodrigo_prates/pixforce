from ClassImage import ClassImage
import os


classifier = ClassImage()
image_folder = os.path.sep.join([os.getcwd(), 'images'])
dataset_folder = os.path.sep.join([os.getcwd(), 'dataset'])
classifier.split_data_from_folder(image_folder, dataset_folder, 0.7, True, '1', 2)