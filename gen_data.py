from ClassImage import ClassImage
import os


classifier = ClassImage()
data_folder = os.path.sep.join([os.getcwd(), 'data'])
output_folder = os.path.sep.join([os.getcwd(), 'images'])

print(classifier.gen_data(data_folder, output_folder))