# README #

Binary classifier for pixforce challenge

### How to Use ###

* Install requirement dependencies in a new env.

* Run gen_data.py script to generate all images.
* Run split_data.py script to separate in train/validation/test datasets
* Run train.py script to train a VGG16 CNN model for binary classification with transfer-learning and fine-tunning.