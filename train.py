from ClassImage import ClassImage
import os


classifier = ClassImage()
dataset_folder = os.path.sep.join([os.getcwd(), 'dataset'])
modelname = 'VGG16'
batchsize = 8
numepochs_head = 60
numepochs_body = 20
num_classes = 2
num_train_files = 6226
num_val_files = 1001
num_test_files = 1001
classifier.training(dataset_folder, modelname, batchsize, numepochs_head, numepochs_body, 
                    num_classes, num_train_files, num_val_files, num_test_files)


#[INFO] evaluating after fine-tuning network...
#              precision    recall  f1-score   support
#
#           0       0.70      0.74      0.72       433
#           1       0.46      0.41      0.44       234
#
#    accuracy                           0.63       667
#   macro avg       0.58      0.58      0.58       667
#weighted avg       0.62      0.63      0.62       667